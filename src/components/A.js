import React, { Component } from 'react'
import B from './B'

export default class A extends Component {

    state={
        a:[]
    };
     
    handleClick=(root1)=>
    {
        this.state.a.push(root1.value);
       
        this.setState({a:this.state.a});
       console.log(this.state.a);

    };
    
      render() {
    return (
      <div>
        <h1>This is a parent component</h1>
        <h3>Write a cource name to list</h3>
       <input type="text" id='ip' />
        <button className='btn btn-primary' onClick={()=>this.handleClick(document.getElementById("ip"))}>Enter a Cource Name</button>
        <hr/>
        {!(this.state.a[0]!==undefined) &&<>  <h1>This is child component</h1><h1>There are no cources here..</h1></>}
        { (this.state.a[0]!==undefined) &&<B value1={this.state.a}/>}
 


      </div>
    )
  }
}

